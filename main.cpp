#include <opencv2/opencv.hpp>

using namespace cv;

Scalar& getRGBFromHue(int hue, Scalar& col)
{
    int hi = (float)hue / 60.0f;
    float f = ((float)hue / 60.0f) - hi;

    float q=1.f-f;
    float t=1.f-q;
    if (hi==0) {
        col[2] = 1.f;
        col[1] = t;
        col[0] = 0;
    } else if (hi==1) {
        col[2] = q;
        col[1] = 1.f;
        col[0] = 0;
    } else if (hi==2) {
        col[2] = 0;
        col[1] = 1.f;
        col[0] = t;
    } else if (hi==3) {
        col[2] = 0;
        col[1] = q;
        col[0] = 1.f;
    } else if (hi==4) {
        col[2] = t;
        col[1] = 0;
        col[0] = 1.f;
    } else if (hi==5) {
        col[2] = 1.f;
        col[1] = 0;
        col[0] = q;
    }

    return col;
}

Scalar& getRGBFromHueUC(int hue, Scalar& col) 
{
    int hi = (float)hue / 60.0f;
    float f = ((float)hue / 60.0f) - hi;

    float q=1.f-f;
    float t=1.f-q;
    q=q*255;
    t=t*255;
    if (hi==0) {
        col[2] = 255.f;
        col[1] = t;
        col[0] = 0;
    } else if (hi==1) {
        col[2] = q;
        col[1] = 255.f;
        col[0] = 0;
    } else if (hi==2) {
        col[2] = 0;
        col[1] = 255.f;
        col[0] = t;
    } else if (hi==3) {
        col[2] = 0;
        col[1] = q;
        col[0] = 255.f;
    } else if (hi==4) {
        col[2] = t;
        col[1] = 0;
        col[0] = 255.f;
    } else if (hi==5) {
        col[2] = 255.f;
        col[1] = 0;
        col[0] = q;
    }

    return col;  
}

int main( int argc, char** argv )
{
    Mat src, hsv;

    if ( argc != 2 || !(src=imread(argv[1], 1)).data )
        return -1;

    cvtColor(src, hsv, CV_BGR2HSV);

	Mat hsvShift = hsv;
	//hsv.copyTo(hsvShift);
	printf("mat type = %d (%d,%d,%d)\n", hsvShift.type(), hsvShift.step[0], hsvShift.step[1], hsvShift.step[2]);
	printf("cols = %d, rows = %d, dims = %d\n", hsvShift.cols, hsvShift.rows, hsvShift.dims);
	for (int i = 0;i<hsvShift.cols;i++) {
		for (int j=0;j<hsvShift.rows;j++) {
			((uchar*)(hsvShift.data + hsvShift.step[0] * j))[hsvShift.step[1] * i] += 100;
			((uchar*)(hsvShift.data + hsvShift.step[0] * j))[hsvShift.step[1] * i] %= 179;
			//((uchar*)(hsvShift.data + hsvShift.step[0] * j))[hsvShift.step[1] * i + 1] = 0;
			//((uchar*)(hsvShift.data + hsvShift.step[0] * j))[hsvShift.step[1] * i + 2] = 0;
		}
	}
	Mat hsvShiftBGR;
	cvtColor(hsvShift, hsvShiftBGR, CV_HSV2BGR);
    namedWindow( "hsvshift", 1 );
	imshow( "hsvshift", hsvShiftBGR);

    // Quantize the hue to 30 levels
    // and the saturation to 32 levels
    int hbins = 30, sbins = 32;
    int histSize[] = {hbins, sbins};
    // hue varies from 0 to 179, see cvtColor
    float hranges[] = { 0, 180 };
    // saturation varies from 0 (black-gray-white) to
    // 255 (pure spectrum color)
    float sranges[] = { 0, 256 };
    const float* ranges[] = { hranges, sranges };
    MatND hist;
    // we compute the histogram from the 0-th and 1-st channels
    int channels[] = {0, 1};

    calcHist( &hsv, 1, channels, Mat(), // do not use mask
              hist, 2, histSize, ranges,
              true, // the histogram is uniform
              false );
    double maxVal=0;
    minMaxLoc(hist, 0, &maxVal, 0, 0);

    int scale = 10;
    int imgwidth = hbins*10;
    Mat hueImg = Mat::zeros(20, imgwidth, CV_32FC3);
    Scalar col;
    for (int i=0;i<imgwidth;i++) {
      
        line(hueImg, Point(i,0), Point(i, 20), getRGBFromHue(360.f*(float)i/(float)imgwidth, col));
    }

    namedWindow( "hoobar", 1 );
    imshow( "hoobar", hueImg );

    Scalar col2;
    Mat histImg = Mat::zeros((sbins+1)*scale, hbins*10, CV_8UC3);
    for (int i=0;i<imgwidth;i++) {
      
        line(histImg, Point(i,0), Point(i, scale), getRGBFromHueUC(360.f*(float)i/(float)imgwidth, col2));
    }
	
    for ( int h = 0; h < hbins; h++ )
        for ( int s = 0; s < sbins; s++ )
        {
            float binVal = hist.at<float>(h, sbins -1 -s);
            int intensity = cvRound(binVal*255/maxVal);
            rectangle( histImg, Point(h*scale, (s+1)*scale),
                       Point( (h+1)*scale - 1, (s+2)*scale - 1),
                       Scalar::all(intensity),
                       CV_FILLED );
        }

    namedWindow( "Source", 1 );
    imshow( "Source", src );

    namedWindow( "H-S Histogram", 1 );
    imshow( "H-S Histogram", histImg );
	imwrite(string(argv[1]) + "_hist.png", histImg);
    waitKey();
}
